/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  VIVEK
 * Created: 10 Apr, 2021
 */

CREATE SCHEMA pms;
CREATE TABLE `pms`.`doctors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `phone_number` INT(10) NOT NULL,
  `address` VARCHAR(45) NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);
CREATE TABLE `pms`.`appoinments` (
  `appointment_id` INT NOT NULL AUTO_INCREMENT,
  `doctor_id` INT NOT NULL,
  `start_time` DATETIME NULL,
  `end_time` DATETIME NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT fk_doctor
    FOREIGN KEY (doctor_id) 
        REFERENCES doctors(id)
) ENGINE=INNODB;