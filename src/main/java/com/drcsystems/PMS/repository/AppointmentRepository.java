/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drcsystems.PMS.repository;

import com.drcsystems.PMS.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author VIVEK
 */
public interface AppointmentRepository extends JpaRepository<Appointment, Long>{
    
}
