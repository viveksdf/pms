/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drcsystems.PMS.repository;

import com.drcsystems.PMS.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author VIVEK
 */
public interface DoctorRepository extends JpaRepository<Doctor, Long>{
    
    @Query("SELECT u FROM Doctor u WHERE u.email = ?1")
    public Doctor findByEmail(String email);
    
}
