/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drcsystems.PMS.controller;

import com.drcsystems.PMS.model.Doctor;
import com.drcsystems.PMS.model.WSResponse;
import com.drcsystems.PMS.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author VIVEK
 */

@RestController
public class DoctorController {
    
    @Autowired
    DoctorRepository doctorRepository;
    
    @PostMapping("/register")
    public WSResponse register(Doctor doctor) {  
        WSResponse response = new WSResponse();
        Doctor docExists = doctorRepository.findByEmail(doctor.getEmail());
        
        if(docExists != null){
           response.setStatus(HttpStatus.METHOD_NOT_ALLOWED);
           response.setError(new Error("User already exists"));
        }
        else
        {
            doctorRepository.save(doctor);
            response.setStatus(HttpStatus.CREATED);
            response.setResponse(doctor);
        }
        return response;
    } 
}
